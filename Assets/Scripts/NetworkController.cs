﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkController : MonoBehaviour {

	string data;
	public string poseURL;
	public TVRTrackingData TVRData;

	// Use this for initialization
	void Start () {
		data = "null";
		TVRData = new TVRTrackingData();
	}

	// Update is called once per frame
	void Update () {
		StartCoroutine("GetPose");
	}

	IEnumerator GetPose(){
		using (UnityWebRequest request = UnityWebRequest.Get("http://127.0.0.1:5603/pose/")){
			yield return request.SendWebRequest();

			if (request.isNetworkError || request.isHttpError){
				Debug.Log(request.error);
			} else {
				//Debug.Log(request.downloadHandler.text);;
				data = request.downloadHandler.text;
				JsonUtility.FromJsonOverwrite(data,TVRData);
				//Debug.Log(TVRData.toString());
			}


		}
	}

}

[System.Serializable]
public class TVRTrackingData {
	public double[] r = {0,0,0};
	public double[] t = {0,0,0};

	public static TVRTrackingData CreateFromJSON(string jsonString){
		return JsonUtility.FromJson<TVRTrackingData>(jsonString);
	}

	public string toString(){
		string s = "r: [" + r[0] + "," + r[1] + "," + r[2] + "]";
		s = s + " \nt: [" + t[0] + "," + t[1] + "," + t[2] + "]"; 
		return s;
	}
	

}