﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerPose : MonoBehaviour {

	public GameObject keyboard;

	private Movement keyboardMovement;

	// Use this for initialization
	void Start () {
		this.keyboardMovement = keyboard.GetComponent<Movement> ();
	}
	
	// Update is called once per frame
	void Update () {
		OVRInput.Update();
		this.transform.position = OVRInput.GetLocalControllerPosition(OVRInput.Controller.RTouch);
		this.transform.rotation = OVRInput.GetLocalControllerRotation (OVRInput.Controller.RTouch);
 
		if(OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, OVRInput.Controller.RTouch) > 0.8)
		{
			//Debug.Log ("Triggered");
			keyboardMovement.posOffset = -keyboard.transform.position + this.transform.position;
		}



		if (OVRInput.Get(OVRInput.Button.One,OVRInput.Controller.RTouch)){
			keyboardMovement.posOffset += new Vector3 (0.0f, -0.01f,0.0f);
		}			
		
		if (OVRInput.Get(OVRInput.Button.Two,OVRInput.Controller.RTouch)){
			keyboardMovement.posOffset += new Vector3 (0.0f, 0.01f,0.0f);
		}

		keyboardMovement.rotOffset += new Vector3 (0, OVRInput.Get (OVRInput.Axis2D.PrimaryThumbstick)[0]);

	}
}
