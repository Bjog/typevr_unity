﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

	public NetworkController nC;
	public Vector3 posOffset;
	public Vector3 rotOffset;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		XZLockedPositionalUpdate();
	
	}

	void RawPositionalUpdate(){
		this.transform.position = posOffset + new Vector3(
			(float)nC.TVRData.t[0],
			(float)nC.TVRData.t[2],
			(float)nC.TVRData.t[1])/1000;//millimetres

		this.transform.localEulerAngles  = rotOffset + new Vector3(
			Mathf.Rad2Deg*(float)nC.TVRData.r[0],
			Mathf.Rad2Deg*(float)nC.TVRData.r[2],
			Mathf.Rad2Deg*(float)nC.TVRData.r[1]);

	}

	void PositionalUpdate(){
		this.transform.position = posOffset + new Vector3(
			-(float)nC.TVRData.t[0],
			-(float)nC.TVRData.t[2],
			(float)nC.TVRData.t[1])/1000;//millimetres

		this.transform.localEulerAngles  = rotOffset + new Vector3(
			Mathf.Rad2Deg*(float)nC.TVRData.r[0],
			Mathf.Rad2Deg*(float)nC.TVRData.r[2],
			-Mathf.Rad2Deg*(float)nC.TVRData.r[1]);
	}

	void XZLockedPositionalUpdate(){
		this.transform.position = posOffset + new Vector3(
			-(float)nC.TVRData.t[0],
			0,
			(float)nC.TVRData.t[1])/1000;//millimetres

		this.transform.localEulerAngles  = rotOffset + new Vector3(
			0,
			Mathf.Rad2Deg*(float)nC.TVRData.r[2],
			0);
	}

//	this.transform.position = posOffset + new Vector3(
//		(float)nC.TVRData.t[0],
//		(float)nC.TVRData.t[2],
//		(float)nC.TVRData.t[1])/1000;//millimetres
//
//	this.transform.localEulerAngles  = rotOffset + new Vector3(
//		Mathf.Rad2Deg*(float)nC.TVRData.r[0],
//		Mathf.Rad2Deg*(float)nC.TVRData.r[2],
//		Mathf.Rad2Deg*(float)nC.TVRData.r[1]);
}
