﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class TextureController : MonoBehaviour {
	Texture2D errorTex;
	Texture2D tex;
	bool loading;
	bool errorloaded;
	public string path = "E:\\Documents\\typewritvr\\typewritvr\\";

	// Use this for initialization
	void Start () {
		loading = false;
		errorloaded = false;
		tex = new Texture2D (4, 4);


		if (!errorloaded) {
//			StartCoroutine ("LoadErrorTex");
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(!loading && errorloaded){
//			StartCoroutine ("LoadTex");
		}

		LoadTextureFromFile(path);
	}

	void LoadTextureFromFile(string path){
		
		byte[] imgdata;

		if (File.Exists (path + "texture.jpg")) {


			/*
			if(File.Exists(path + "tex.jpg")){
				File.Delete (path + "tex.jpg");
			}

			File.Copy (path + "texture.jpg", path + "tex.jpg");
			*/
			try{
			imgdata = File.ReadAllBytes (path + "texture.jpg");
			
			tex.LoadImage (imgdata);
			tex.mipMapBias = -2.0F;
			tex.anisoLevel = 9;
			tex.filterMode = FilterMode.Point;
				tex.Apply();

			if ((tex.width > 8 && tex.height > 8)) {
				GetComponent<Renderer> ().material.mainTexture = tex;
			}
			} catch(IOException){
				//Acceptable error

			}


		}
	}

	IEnumerator LoadErrorTex(){
		errorTex = new Texture2D (1,1, TextureFormat.DXT1, false, false);
		using (WWW errorwww = new WWW ("http://localhost:5603/error/")) {
			yield return errorwww;
			errorwww.LoadImageIntoTexture(errorTex);
		}
		errorloaded = true;
	}

	IEnumerator LoadTex()
	{
		loading = true;

		Texture2D tex = new Texture2D (4, 4, TextureFormat.DXT1, false, false);
		// Start a download of the given URL
		using (WWW www = new WWW("http://localhost:5603/texture/"))
		{
			// Wait for download to complete
			yield return www;

			if (www.texture != null) {
				if (www.texture.width != errorTex.width) {
					www.LoadImageIntoTexture (tex);
					// assign texture
					GetComponent<Renderer> ().material.mainTexture = tex;	
				}
			}
		}

		loading = false;
	}
		
}
