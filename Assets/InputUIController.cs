﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputUIController : MonoBehaviour {

	public InputField if1,if2,if3;
	public KeyboardController kbc;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void setScale(){
		float x = float.Parse (if1.text);
		float y = float.Parse (if2.text);
		float z = float.Parse (if3.text);

		kbc.setScaleMM (x, y, z);
	}
}
