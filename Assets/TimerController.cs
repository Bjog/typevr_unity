﻿using System.Collections;
using System.Collections.Generic;
using System.IO;

using UnityEngine;
using UnityEngine.UI;


public class TimerController : MonoBehaviour {

	int current;
	float tStart, tEnd, tDiff;
	bool counting, timing;

	public Text VrText;
	public InputField nameField;
	private string path = "data.txt";

	// Use this for initialization
	void Start () {
		tStart = tEnd = tDiff = 0.0f;
		counting = timing = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space) && timing) {
			stopTimer ();
		}

		if (OVRInput.Get(OVRInput.Button.One,OVRInput.Controller.LTouch) && !counting) {

			StartCoroutine ("Countdown", 3);

		}
		
	}

	void setCountdownVal(int val){
		VrText.text = "Press SPACEBAR in " + val.ToString ();
	}

	void setText(string s){
		VrText.text = s;

	}

	void startTimer(){

		timing = true;
		VrText.color = new Color32 (70, 255, 140, 255);
		setText ("PRESS SPACEBAR");

		tStart = Time.time;
		//Debug.Log ("TIMER START");

	}

	void stopTimer(){
		tEnd = Time.time;
		tDiff = tEnd - tStart;
		timing = false;

		setText ("");
		Debug.Log ("Time Taken = " + tDiff + "s");

		StreamWriter sWriter = new StreamWriter (path,true);
		sWriter.WriteLine (nameField.text.ToString() + "\t" + tDiff + "s\t" + System.DateTime.Now.ToShortDateString() +
			"\t" + System.DateTime.Now.ToShortTimeString());
		sWriter.Close ();

	}


	public IEnumerator Countdown(int start){
		counting = true;
		VrText.transform.gameObject.SetActive (true);
		VrText.color = new Color32 (255, 70, 130, 255);
		current = start;

		while (current > 0) {
			setCountdownVal (current);
			yield return new WaitForSeconds (1.0f);
			current--;
		}
		counting = false;

		startTimer ();


	}
}
