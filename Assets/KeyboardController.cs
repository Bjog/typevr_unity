﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyboardController : MonoBehaviour {
	bool cooling;
	public Text visibilityText;


	// Use this for initialization
	void Start () {
		cooling = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (OVRInput.Get (OVRInput.Button.Two, OVRInput.Controller.LTouch) && !cooling) {
			this.GetComponent<MeshRenderer>().enabled = (!this.GetComponent<MeshRenderer>().enabled);

			foreach (Transform t in transform) {
				t.gameObject.SetActive (!t.gameObject.activeSelf );

			}

			if (this.GetComponent<MeshRenderer> ().enabled) {
				visibilityText.text = "VISIBLE";
			} else {
				visibilityText.text = "HIDDEN";
			}
			


			StartCoroutine ("Cooldown", 2);
		}
	}

	public IEnumerator Cooldown(float val){
		cooling = true;
		yield return new WaitForSeconds(val);
		cooling = false;

	}

	public void setScaleMM(float x, float y, float z){
		this.transform.localScale = new Vector3 (x, y, z) / 1000.0f;
	}
}
