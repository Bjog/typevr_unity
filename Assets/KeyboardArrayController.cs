﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityRawInput;

public class KeyboardArrayController : MonoBehaviour {



	// Use this for initialization
	void Start () {
		//Hide Children
		MeshRenderer[] keyRenderers = GetComponentsInChildren<MeshRenderer>();
		for (int i = 0; i < keyRenderers.Length; i++) {
			keyRenderers [i].enabled = false;
		}

		RawKeyInput.Start (true);


		RawKeyInput.OnKeyUp += HandleKeyUp;
		RawKeyInput.OnKeyDown += HandleKeyDown;
	}
	
	// Update is called once per frame
	void Update () {
		//HandlerKeyDown ();
		//HandlerKeyUp ();

	}

	void HandleKeyUp(RawKey key){
		HandlerKeyUp (key);
	}

	void HandleKeyDown(RawKey key){
		HandlerKeyDown (key);
	}


	void HandlerKeyUp(){
		if (Input.GetKeyUp (KeyCode.Q)) {
			GameObject.Find ("Q").GetComponent<MeshRenderer> ().enabled = false;
		}
		if (Input.GetKeyUp (KeyCode.W)) {
			GameObject.Find ("W").GetComponent<MeshRenderer> ().enabled = false;
		}
		if (Input.GetKeyUp (KeyCode.E)) {
			GameObject.Find ("E").GetComponent<MeshRenderer> ().enabled = false;
		}
		if (Input.GetKeyUp (KeyCode.R)) {
			GameObject.Find ("R").GetComponent<MeshRenderer> ().enabled = false;
		}
		if (Input.GetKeyUp (KeyCode.T)) {
			GameObject.Find ("T").GetComponent<MeshRenderer> ().enabled = false;
		}
		if (Input.GetKeyUp (KeyCode.Y)) {
			GameObject.Find ("Y").GetComponent<MeshRenderer> ().enabled = false;
		}
		if (Input.GetKeyUp (KeyCode.U)) {
			GameObject.Find ("U").GetComponent<MeshRenderer> ().enabled = false;
		}
		if (Input.GetKeyUp (KeyCode.I)) {
			GameObject.Find ("I").GetComponent<MeshRenderer> ().enabled = false;
		}
		if (Input.GetKeyUp (KeyCode.O)) {
			GameObject.Find ("O").GetComponent<MeshRenderer> ().enabled = false;
		}
		if (Input.GetKeyUp (KeyCode.P)) {
			GameObject.Find ("P").GetComponent<MeshRenderer> ().enabled = false;
		}
		if (Input.GetKeyUp (KeyCode.A)) {
			GameObject.Find ("A").GetComponent<MeshRenderer> ().enabled = false;
		}
		if (Input.GetKeyUp (KeyCode.S)) {
			GameObject.Find ("S").GetComponent<MeshRenderer> ().enabled = false;
		}
		if (Input.GetKeyUp (KeyCode.D)) {
			GameObject.Find ("D").GetComponent<MeshRenderer> ().enabled = false ;
		}
		if (Input.GetKeyUp (KeyCode.F)) {
			GameObject.Find ("F").GetComponent<MeshRenderer> ().enabled = false ;
		}
		if (Input.GetKeyUp (KeyCode.G)) {
			GameObject.Find ("G").GetComponent<MeshRenderer> ().enabled = false ;
		}
		if (Input.GetKeyUp (KeyCode.H)) {
			GameObject.Find ("H").GetComponent<MeshRenderer> ().enabled = false ;
		}
		if (Input.GetKeyUp (KeyCode.J)) {
			GameObject.Find ("J").GetComponent<MeshRenderer> ().enabled = false ;
		}
		if (Input.GetKeyUp (KeyCode.K)) {
			GameObject.Find ("K").GetComponent<MeshRenderer> ().enabled = false ;
		}
		if (Input.GetKeyUp (KeyCode.L)) {
			GameObject.Find ("L").GetComponent<MeshRenderer> ().enabled = false ;
		}
		if (Input.GetKeyUp (KeyCode.Z)) {
			GameObject.Find ("Z").GetComponent<MeshRenderer> ().enabled = false ;
		}
		if (Input.GetKeyUp (KeyCode.X)) {
			GameObject.Find ("X").GetComponent<MeshRenderer> ().enabled = false ;
		}
		if (Input.GetKeyUp (KeyCode.C)) {
			GameObject.Find ("C").GetComponent<MeshRenderer> ().enabled = false ;
		}
		if (Input.GetKeyUp (KeyCode.V)) {
			GameObject.Find ("V").GetComponent<MeshRenderer> ().enabled = false ;
		}
		if (Input.GetKeyUp (KeyCode.B)) {
			GameObject.Find ("B").GetComponent<MeshRenderer> ().enabled = false ;
		}
		if (Input.GetKeyUp (KeyCode.N)) {
			GameObject.Find ("N").GetComponent<MeshRenderer> ().enabled = false ;
		}
		if (Input.GetKeyUp (KeyCode.M)) {
			GameObject.Find ("M").GetComponent<MeshRenderer> ().enabled = false ;
		}
		if (Input.GetKeyUp (KeyCode.Space)) {
			GameObject.Find ("SPACE").GetComponent<MeshRenderer> ().enabled = false ;
		}


	}

	void HandlerKeyUp(RawKey k){
		if (k == RawKey.Q) {
			GameObject.Find ("Q").GetComponent<MeshRenderer> ().enabled = false;
		}
		if (k == RawKey.W) {
			GameObject.Find ("W").GetComponent<MeshRenderer> ().enabled = false;
		}
		if (k == RawKey.E) {
			GameObject.Find ("E").GetComponent<MeshRenderer> ().enabled = false;
		}
		if (k == RawKey.R) {
			GameObject.Find ("R").GetComponent<MeshRenderer> ().enabled = false;
		}
		if (k == RawKey.T) {
			GameObject.Find ("T").GetComponent<MeshRenderer> ().enabled = false;
		}
		if (k == RawKey.Y) {
			GameObject.Find ("Y").GetComponent<MeshRenderer> ().enabled = false;
		}
		if (k == RawKey.U) {
			GameObject.Find ("U").GetComponent<MeshRenderer> ().enabled = false;
		}
		if (k == RawKey.I) {
			GameObject.Find ("I").GetComponent<MeshRenderer> ().enabled = false;
		}
		if (k == RawKey.O) {
			GameObject.Find ("O").GetComponent<MeshRenderer> ().enabled = false;
		}
		if (k == RawKey.P) {
			GameObject.Find ("P").GetComponent<MeshRenderer> ().enabled = false;
		}
		if (k == RawKey.A) {
			GameObject.Find ("A").GetComponent<MeshRenderer> ().enabled = false;
		}
		if (k == RawKey.S) {
			GameObject.Find ("S").GetComponent<MeshRenderer> ().enabled = false;
		}
		if (k == RawKey.D) {
			GameObject.Find ("D").GetComponent<MeshRenderer> ().enabled = false ;
		}
		if (k == RawKey.F) {
			GameObject.Find ("F").GetComponent<MeshRenderer> ().enabled = false ;
		}
		if (k == RawKey.G) {
			GameObject.Find ("G").GetComponent<MeshRenderer> ().enabled = false ;
		}
		if (k == RawKey.H) {
			GameObject.Find ("H").GetComponent<MeshRenderer> ().enabled = false ;
		}
		if (k == RawKey.J) {
			GameObject.Find ("J").GetComponent<MeshRenderer> ().enabled = false ;
		}
		if (k == RawKey.K) {
			GameObject.Find ("K").GetComponent<MeshRenderer> ().enabled = false ;
		}
		if (k == RawKey.L) {
			GameObject.Find ("L").GetComponent<MeshRenderer> ().enabled = false ;
		}
		if (k == RawKey.Z) {
			GameObject.Find ("Z").GetComponent<MeshRenderer> ().enabled = false ;
		}
		if (k == RawKey.X) {
			GameObject.Find ("X").GetComponent<MeshRenderer> ().enabled = false ;
		}
		if (k == RawKey.C) {
			GameObject.Find ("C").GetComponent<MeshRenderer> ().enabled = false ;
		}
		if (k == RawKey.V) {
			GameObject.Find ("V").GetComponent<MeshRenderer> ().enabled = false ;
		}
		if (k == RawKey.B) {
			GameObject.Find ("B").GetComponent<MeshRenderer> ().enabled = false ;
		}
		if (k == RawKey.N) {
			GameObject.Find ("N").GetComponent<MeshRenderer> ().enabled = false ;
		}
		if (k == RawKey.M) {
			GameObject.Find ("M").GetComponent<MeshRenderer> ().enabled = false ;
		}
		if (k == RawKey.Space) {
			GameObject.Find ("SPACE").GetComponent<MeshRenderer> ().enabled = false ;
		}


	}

	void HandlerKeyDown(){
		if (Input.GetKeyDown (KeyCode.Q)) {
			GameObject.Find ("Q").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (Input.GetKeyDown (KeyCode.W)) {
			GameObject.Find ("W").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (Input.GetKeyDown (KeyCode.E)) {
			GameObject.Find ("E").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (Input.GetKeyDown (KeyCode.R)) {
			GameObject.Find ("R").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (Input.GetKeyDown (KeyCode.T)) {
			GameObject.Find ("T").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (Input.GetKeyDown (KeyCode.Y)) {
			GameObject.Find ("Y").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (Input.GetKeyDown (KeyCode.U)) {
			GameObject.Find ("U").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (Input.GetKeyDown (KeyCode.I)) {
			GameObject.Find ("I").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (Input.GetKeyDown (KeyCode.O)) {
			GameObject.Find ("O").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (Input.GetKeyDown (KeyCode.P)) {
			GameObject.Find ("P").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (Input.GetKeyDown (KeyCode.A)) {
			GameObject.Find ("A").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (Input.GetKeyDown (KeyCode.S)) {
			GameObject.Find ("S").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (Input.GetKeyDown (KeyCode.D)) {
			GameObject.Find ("D").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (Input.GetKeyDown (KeyCode.F)) {
			GameObject.Find ("F").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (Input.GetKeyDown (KeyCode.G)) {
			GameObject.Find ("G").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (Input.GetKeyDown (KeyCode.H)) {
			GameObject.Find ("H").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (Input.GetKeyDown (KeyCode.J)) {
			GameObject.Find ("J").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (Input.GetKeyDown (KeyCode.K)) {
			GameObject.Find ("K").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (Input.GetKeyDown (KeyCode.L)) {
			GameObject.Find ("L").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (Input.GetKeyDown (KeyCode.Z)) {
			GameObject.Find ("Z").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (Input.GetKeyDown (KeyCode.X)) {
			GameObject.Find ("X").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (Input.GetKeyDown (KeyCode.C)) {
			GameObject.Find ("C").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (Input.GetKeyDown (KeyCode.V)) {
			GameObject.Find ("V").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (Input.GetKeyDown (KeyCode.B)) {
			GameObject.Find ("B").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (Input.GetKeyDown (KeyCode.N)) {
			GameObject.Find ("N").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (Input.GetKeyDown (KeyCode.M)) {
			GameObject.Find ("M").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (Input.GetKeyDown (KeyCode.Space)) {
			GameObject.Find ("SPACE").GetComponent<MeshRenderer> ().enabled = true ;
		}

	}

	void HandlerKeyDown(RawKey k){
		if (k == RawKey.Q) {
			GameObject.Find ("Q").GetComponent<MeshRenderer> ().enabled = true;
		}
		if (k == RawKey.W) {
			GameObject.Find ("W").GetComponent<MeshRenderer> ().enabled = true;
		}
		if (k == RawKey.E) {
			GameObject.Find ("E").GetComponent<MeshRenderer> ().enabled = true;
		}
		if (k == RawKey.R) {
			GameObject.Find ("R").GetComponent<MeshRenderer> ().enabled = true;
		}
		if (k == RawKey.T) {
			GameObject.Find ("T").GetComponent<MeshRenderer> ().enabled = true;
		}
		if (k == RawKey.Y) {
			GameObject.Find ("Y").GetComponent<MeshRenderer> ().enabled = true;
		}
		if (k == RawKey.U) {
			GameObject.Find ("U").GetComponent<MeshRenderer> ().enabled = true;
		}
		if (k == RawKey.I) {
			GameObject.Find ("I").GetComponent<MeshRenderer> ().enabled = true;
		}
		if (k == RawKey.O) {
			GameObject.Find ("O").GetComponent<MeshRenderer> ().enabled = true;
		}
		if (k == RawKey.P) {
			GameObject.Find ("P").GetComponent<MeshRenderer> ().enabled = true;
		}
		if (k == RawKey.A) {
			GameObject.Find ("A").GetComponent<MeshRenderer> ().enabled = true;
		}
		if (k == RawKey.S) {
			GameObject.Find ("S").GetComponent<MeshRenderer> ().enabled = true;
		}
		if (k == RawKey.D) {
			GameObject.Find ("D").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (k == RawKey.F) {
			GameObject.Find ("F").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (k == RawKey.G) {
			GameObject.Find ("G").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (k == RawKey.H) {
			GameObject.Find ("H").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (k == RawKey.J) {
			GameObject.Find ("J").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (k == RawKey.K) {
			GameObject.Find ("K").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (k == RawKey.L) {
			GameObject.Find ("L").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (k == RawKey.Z) {
			GameObject.Find ("Z").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (k == RawKey.X) {
			GameObject.Find ("X").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (k == RawKey.C) {
			GameObject.Find ("C").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (k == RawKey.V) {
			GameObject.Find ("V").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (k == RawKey.B) {
			GameObject.Find ("B").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (k == RawKey.N) {
			GameObject.Find ("N").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (k == RawKey.M) {
			GameObject.Find ("M").GetComponent<MeshRenderer> ().enabled = true ;
		}
		if (k == RawKey.Space) {
			GameObject.Find ("SPACE").GetComponent<MeshRenderer> ().enabled = true ;
		}


	}
}
